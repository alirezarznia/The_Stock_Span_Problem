//include <god>
/*                   ___    ____    ___    ___
      /\     |        |    |____|  |         /     /\
     /__\    |        |    |  \    |---     /     /__\
    /    \   |___    _|_   |   \   |___    /__   /    \
*/
#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>

#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP 			make_pair
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define     GSORT(x)          sort(ALL(x), greater<typeof(*((x).begin()))>())
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define     UNIQUE(v)         Sort(v); (v).resize(unique(ALL(v)) - (v).begin())
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62
#define     SF                scanf
#define     PF                printf
#define     F                   first
#define     S                   second
#define boost  ios_base::sync_with_stdio(NULL); cin.tie(NULL); cout.tie(NULL);


typedef long long ll;
typedef long double ld;

using namespace std;
ll arr[1000001] , L[1000001];
int main(){
   // Test;
    stack<ll>st;
    ll t ;cin >>t;
    while(t--){
        while(!st.empty()) st.pop();
        ll n ; cin>>n;
        Rep(i, n) cin>>arr[i];
        st.push(0);
        L[0]=1;
        for(int i = 0;  i<n ;st.push(i++)){
            while(!st.empty() && arr[st.top()] <= arr[i]) st.pop();
            if(st.empty()) L[i] = i+1;
            else  L[i] = i - st.top()   ;
        }
        Rep(i , n ) cout<< L[i]<<" ";
        cout<<endl;
    }
}
